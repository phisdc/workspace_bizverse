
const nextBtn = $(".next-btn");
const prevBtn = $(".prev-btn");
const slides = $(".slide");
const numberOfSlides = slides.length;
var slideNumber = 0;

const tabs = $(".tab-item");
const progressBars = $(".tab-item__progress-bar");

const mobile_imgs = $(".slide-img-mobile");

tabs.each((index) => {
  $(tabs[index]).mouseover(() => {
    clearInterval(playSlider);
    slides.each((index) => {
      $(slides[index]).removeClass("active");
    });
    mobile_imgs.each((index) => {
      $(mobile_imgs[index]).removeClass("active");
    });
    tabs.each((index) => {
      $(tabs[index]).removeClass("active");
    });
    progressBars.each((index) => {
      $(progressBars[index]).removeClass("active");
      $(progressBars[index]).removeClass("run-progress-bar");
    });

    slideNumber = index;

    if (slideNumber > (numberOfSlides - 1)) {
      slideNumber = 0;
    }
    else if (slideNumber < 0) {
      slideNumber = numberOfSlides - 1;
    }

    $(tabs[slideNumber]).addClass("active");
    $(slides[slideNumber]).addClass("active");
    $(progressBars[slideNumber]).addClass("active");
    $(mobile_imgs[slideNumber]).addClass("active");
  });
  $(tabs[index]).mouseout(() => {
    $(".tab-item__progress-bar.active").addClass("run-progress-bar");
    progressBars.each((index) => {
      $(progressBars[index]).removeClass("active");
    });
    repeater();
  });
})


//image slider next button
nextBtn.click(() => {
  slides.each((index) => {
    $(slides[index]).removeClass("active");
  });
  tabs.each((index) => {
    $(tabs[index]).removeClass("active");
  });
  progressBars.each((index) => {
    $(progressBars[index]).removeClass("run-progress-bar");
  });
  mobile_imgs.each((index) => {
    $(mobile_imgs[index]).removeClass("active");
  });

  slideNumber++;

  if (slideNumber > (numberOfSlides - 1)) {
    slideNumber = 0;
  }

  $(slides[slideNumber]).addClass("active");
  $(tabs[slideNumber]).addClass("active");
  $(progressBars[slideNumber]).addClass("run-progress-bar");
  $(mobile_imgs[slideNumber]).addClass("active");
});

//image slider previous button
prevBtn.click(() => {
  slides.each((index) => {
    $(slides[index]).removeClass("active");
  });
  tabs.each((index) => {
    $(tabs[index]).removeClass("active");
  });
  progressBars.each((index) => {
    $(progressBars[index]).removeClass("run-progress-bar");
  });
  mobile_imgs.each((index) => {
    $(mobile_imgs[index]).removeClass("active");
  });
  // slideIcons.forEach((slideIcon) => {
  //   slideIcon.classList.remove("active");
  // });

  slideNumber--;

  if (slideNumber < 0) {
    slideNumber = numberOfSlides - 1;
  }

  $(slides[slideNumber]).addClass("active");
  $(tabs[slideNumber]).addClass("active");
  $(progressBars[slideNumber]).addClass("run-progress-bar");
  $(mobile_imgs[slideNumber]).addClass("active");
  // slideIcons[slideNumber].classList.add("active");
});

//image slider autoplay
var playSlider;

var repeater = () => {
  playSlider = setInterval(function () {
    slides.each((index) => {
      $(slides[index]).removeClass("active");
    });
    tabs.each((index) => {
      $(tabs[index]).removeClass("active");
    });
    mobile_imgs.each((index) => {
      $(mobile_imgs[index]).removeClass("active");
    });
    progressBars.each((index) => {
      $(progressBars[index]).removeClass("run-progress-bar");
    });

    slideNumber++;

    if (slideNumber > (numberOfSlides - 1)) {
      slideNumber = 0;
    }

    $(slides[slideNumber]).addClass("active");
    $(mobile_imgs[slideNumber]).addClass("active");
    $(tabs[slideNumber]).addClass("active");
    $(progressBars[slideNumber]).addClass("run-progress-bar");
  }, 4000);
}
repeater();



//stop the image slider autoplay on mouseover
// slider.addEventListener("mouseover", () => {
//   clearInterval(playSlider);
// });

//start the image slider autoplay again on mouseout
// slider.addEventListener("mouseout", () => {
//   repeater();
// });
