const selected = document.querySelector(".selected");
const selected_text = document.querySelector(".selected .selected-text");
const selected_img = document.querySelector(".selected .selected-img");
const selectbox_inner = document.querySelector(".select-box__inner");

const optionsList = document.querySelectorAll(".option");

selected.addEventListener("click", () => {
  selectbox_inner.classList.toggle("active");
});
optionsList.forEach(o => {
  o.addEventListener("click", () => {
    selected_text.innerHTML = o.querySelector("label").innerHTML;
    selectbox_inner.classList.remove("active");
    selected_img.src = o.querySelector("img").src;
  });
});

$('.quantity').each(function() {
  var spinner = $(this),
      input = spinner.find('input[type="number"]'),
      btnUp = spinner.find('.quantity__btn--up'),
      btnDown = spinner.find('.quantity__btn--down'),
      min = input.attr('min'),
      max = input.attr('max');

  btnUp.click(function() {
    var oldValue = parseFloat(input.val());
    if (oldValue >= max) {
      var newVal = oldValue;
    } else {
      var newVal = oldValue + 1;
    }
    spinner.find("input").val(newVal);
    spinner.find("input").trigger("change");
  });

  btnDown.click(function() {
    var oldValue = parseFloat(input.val());
    if (oldValue <= min) {
      var newVal = oldValue;
    } else {
      var newVal = oldValue - 1;
    }
    spinner.find("input").val(newVal);
    spinner.find("input").trigger("change");
  });
});