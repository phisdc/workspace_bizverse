const model = $(".model");
const btnOpen = $(".open-model-btn");
const btnClose = $(".model__inner__border .close-modal");
const btnClose2 = $(".request-success-popup .close-modal");
const step1__selects = $(".step-1__select");
const step2__selects = $(".step-2__select");
const form = $('.multi-step-form');

const pages = $(".page");
const model_border = $(".model__inner__border");
const popup_notification = $(".request-success-popup__border");
// const slidePage = $(".slide-page");
const nextBtnFirst = $(".firstNext");
const prevBtnSec = $(".prev-1");
const nextBtnSec = $(".next-1");
const prevBtnThird = $(".prev-2");
const nextBtnThird = $(".next-2");
const prevBtnFourth = $(".prev-3");
const submitBtn = $(".submit-modal");
const bullet = $(".step .bullet");

const price_next = $(".price-table-nav .next-btn");
const price_prev = $(".price-table-nav .prev-btn");
const price_table_modal = $(".page .bizworkspace-tables .price-table");
const numberOfPriceTable = price_table_modal.length;

let current_priceTable = 0;

price_prev.click(() => {
  price_table_modal.each((index) => {
    $(price_table_modal[index]).removeClass("active");
  });
  current_priceTable--;

  if (current_priceTable < 0) {
    current_priceTable = numberOfPriceTable - 1;
  }

  $(price_table_modal[current_priceTable]).addClass("active");
})

price_next.click(() => {
  price_table_modal.each((index) => {
    $(price_table_modal[index]).removeClass("active");
  });
  current_priceTable++;

  if (current_priceTable > (numberOfPriceTable - 1)) {
    current_priceTable = 0;
  }

  $(price_table_modal[current_priceTable]).addClass("active");
})

const feature_next = $(".feature-table-nav .next-btn");
const feature_prev = $(".feature-table-nav .prev-btn");
const feature_table_modal = $(".page .bizworkspace-tables .feature-table");
const numberOfFeatureTable = feature_table_modal.length;

let current_featureTable = 0;

feature_prev.click(() => {
  feature_table_modal.each((index) => {
    $(feature_table_modal[index]).removeClass("active");
  });
  current_featureTable--;

  if (current_featureTable < 0) {
    current_featureTable = numberOfFeatureTable - 1;
  }

  $(feature_table_modal[current_featureTable]).addClass("active");
})

feature_next.click(() => {
  feature_table_modal.each((index) => {
    $(feature_table_modal[index]).removeClass("active");
  });
  current_featureTable++;

  if (current_featureTable > (numberOfFeatureTable - 1)) {
    current_featureTable = 0;
  }

  $(feature_table_modal[current_featureTable]).addClass("active");
})

let current = 1;

let registerData = {
  package: '',
  feature: '',
  logo: 'default logo',
  wsdomain: '',
  employees_nums: 0,
  business: {
    business_name: '',
    business_img: ''
  }
}

var btnUploadLogo = $(".upload-logo");
var btnUploadLogoInput = $(".upload-logo-input");

btnUploadLogo.click(function (e) { // ??
  btnUploadLogoInput.click()
});

function toggleModel() {
  model.toggleClass("hide");
}
function toggleSelect(select) { //?
  select.toggleClass("active");
}

btnOpen.click(toggleModel);
btnClose.click(function () {
  setTimeout(function () {
    location.reload();
  }, 0);
});
btnClose2.click(function () {
  setTimeout(function () {
    // model_border.removeClass("hide");
    // popup_notification.addClass("hide");
    location.reload();
  }, 0);
});
submitBtn.click(function () {
  popup_notification.removeClass("hide");
  model_border.addClass("hide");
});

// let model_height = ['1630px', '1090px', '700px', '840px'];
$(window).resize(function() {
  var width = $(window).width();
  var priceTables = $(".page .price-table");
  var featureTables = $(".page .feature-table");
  if (width < 1201){
    priceTables.each(index => {
      $(priceTables[index]).removeClass("col-3");
    })
    featureTables.each(index => {
      $(featureTables[index]).removeClass("col-2dot4");
    })
  }
  else {
    priceTables.each(index => {
      $(priceTables[index]).addClass("col-3");
    })
    featureTables.each(index => {
      $(featureTables[index]).addClass("col-2dot4");
    })
  }
});

$(bullet[0]).addClass("active");

nextBtnFirst.click(function (event) {
  event.preventDefault();
  if (checkStep1(step1__selects)) {

    // slidePage.css({ 'margin-left': '-25%' });

    $(bullet[current]).addClass("active");

    current += 1;
    // model_border.css({ 'height': model_height[current - 1] });
    form.css({ 'height': $(pages[current - 1]).height() })
    pages.each((index) => {
      $(pages[index]).removeClass("active");
    })
    $(pages[current - 1]).addClass("active");

    let pakage_index = -1;
    step1__selects.each((index) => {
      if ($(step1__selects[index]).hasClass("active")) { pakage_index = index; return; }
    });
    switch (pakage_index) {
      case 0: registerData.package = "Free"; break;
      case 1: registerData.package = "Basic"; break;
      case 2: registerData.package = "Standard"; break;
      case 3: registerData.package = "Professional"; break;
      default: break;
    }
    $(".label-value.package").html(registerData.package);
  }
  // message
});
nextBtnSec.click(function (event) {
  event.preventDefault();
  if (checkStep2(step2__selects)) {
    // slidePage.css({ 'margin-left': '-50%' });
    $(bullet[current]).addClass("active");
    current += 1;
    // model_border.css({ 'height': model_height[current - 1] });
    form.css({ 'height': $(pages[current - 1]).height() })
    pages.each((index) => {
      $(pages[index]).removeClass("active");
    })
    $(pages[current - 1]).addClass("active");

    let feature_index = -1;
    step2__selects.each((index) => {
      if ($(step2__selects[index]).hasClass("active")) { feature_index = index; return; }
    });
    switch (feature_index) {
      case 0: registerData.feature = "Collaboration"; break;
      case 1: registerData.feature = "CRM"; break;
      case 2: registerData.feature = "Task & Projects"; break;
      case 3: registerData.feature = "Sites & Stores"; break;
      case 4: registerData.feature = "HR & Automation"; break;
      default: break;
    }
    $(".label-value.feature").html(registerData.feature);
  }
  //message
});
nextBtnThird.click(function (event) {
  event.preventDefault();
  if (validateInputs(this)) {
    $(".business-message").removeClass("display");

    // slidePage.css({ 'margin-left': '-75%' });
    $(bullet[current]).addClass("active");
    current += 1;
    // model_border.css({ 'height': model_height[current - 1] });
    form.css({ 'height': $(pages[current - 1]).height() })
    pages.each((index) => {
      $(pages[index]).removeClass("active");
    })
    $(pages[current - 1]).addClass("active");

    const inputs = $(".step3-input");
    registerData.logo = $(".step3-logo").attr('src');
    registerData.wsdomain = $(inputs[0]).val();
    registerData.employees_nums = $(inputs[1]).val();
    registerData.business.business_name = $(".selected .selected-text").html();
    registerData.business.business_img = $(".selected .selected-img").attr('src');
    $(".label-value.wsDomain").html(registerData.wsdomain);
    $(".label-value.employeesNums").html(registerData.employees_nums);
    $(".business-name.business").html(registerData.business.business_name);
    $(".business-logo img").attr('src', registerData.business.business_img);
  }
  else {
    $(".wsDomain-input").change();
    if ($(".selected .selected-text").html() == "")
      $(".business-message").addClass("display");
    else $(".business-message").removeClass("display");
  }
});



prevBtnSec.click(function (event) {
  event.preventDefault();
  // slidePage.css({ 'margin-left': '0%' });

  $(bullet[current - 1]).removeClass("active");
  // model_border.css({ 'height': model_height[current - 2] });
  form.css({ 'height': $(pages[current - 2]).height() })
  pages.each((index) => {
    $(pages[index]).removeClass("active");
  })
  $(pages[current - 2]).addClass("active");

  current -= 1;
});
prevBtnThird.click(function (event) {
  event.preventDefault();
  // slidePage.css({ 'margin-left': '-25%' });
  $(bullet[current - 1]).removeClass("active");
  // model_border.css({ 'height': model_height[current - 2] });
  form.css({ 'height': $(pages[current - 2]).height() })
  pages.each((index) => {
    $(pages[index]).removeClass("active");
  })
  $(pages[current - 2]).addClass("active");

  current -= 1;
});
prevBtnFourth.click(function (event) {
  event.preventDefault();
  // slidePage.css({ 'margin-left': '-50%' });
  $(bullet[current - 1]).removeClass("active");
  // model_border.css({ 'height': model_height[current - 2] });
  form.css({ 'height': $(pages[current - 2]).height() })
  pages.each((index) => {
    $(pages[index]).removeClass("active");
  })
  $(pages[current - 2]).addClass("active");
  current -= 1;
});

//


step1__selects.each(index => {
  $(step1__selects[index]).click(function () {
    step1__selects.each(index1 => {
      $(step1__selects[index1]).removeClass("active");
    })
    $(step1__selects[index]).addClass("active");
  });
})
step2__selects.each(index => {
  $(step2__selects[index]).click(function () {
    step2__selects.each(index2 => {
      $(step2__selects[index2]).removeClass("active");
    })
    $(step2__selects[index]).addClass("active");
  });
})

let checkStep1 = (step1__selects) => {
  let check = false;
  step1__selects.each(index => {
    if ($(step1__selects[index]).hasClass("active")) { check = true; return; }
  });
  return check;
}
let checkStep2 = (step2__selects) => {
  let check = false;
  step2__selects.each(index => {
    if ($(step2__selects[index]).hasClass("active")) { check = true; return; }
  });
  return check;
}

// $('input.step3-input').blur(function() {
//   if(!$.trim(this.value).length) { // zero-length string AFTER a trim
//          $(this).addClass('error');
//   }
// });
function checkValidity(input) {
  if (!$.trim(input.value).length) {
    return false;
  }
  return true;
}

function validateInputs(ths) {
  let inputsValid = true;

  const inputs =
    $(".step3-input");
  for (let i = 0; i < inputs.length; i++) {
    const valid = checkValidity(inputs[i]);
    if (!valid) {
      inputsValid = false;
      $(inputs[i]).addClass("invalid-input");
    } else {
      $(inputs[i]).removeClass("invalid-input");
    }
    if ($(".selected .selected-text").html() == "") inputsValid = false;
  }
  return inputsValid;
}
function validateInput(ths) {
  const valid = checkValidity(ths);
  if (!valid) {
    $("warning-message").addClass("display");
  } else {
    $("warning-message").removeClass("display");
  }
}
$(".wsDomain-input").change(function () {
  const valid = checkValidity(this);
  if (!valid) {
    $(".wsDomain-message").addClass("display");
  } else {
    $(".wsDomain-message").removeClass("display");
  }
})




// function readURL(input) {
//   if (input.files && input.files[0]) {
//     var imgs = $('.upload-logo__img');
//     imgs.forEach(img => {
//       img.src = URL.createObjectURL(input.files[0]);
//     })
//   }
// }
$(document).ready(() => {
  $('#myFile').change(function () {
    const file = this.files[0];
    console.log(file);
    if (file) {
      let reader = new FileReader();
      reader.onload = function (event) {
        console.log(event.target.result);
        $('.upload-logo__img').attr('src', event.target.result);
      }
      reader.readAsDataURL(file);
    }
  });
});

// const select_free = $(".select-free");
// const select_basic = $(".select-basic");
// const select_collab = $(".select-collab");
// const select_pro = $(".select-pro");

$(".select-free").click(function () {
  toggleModel();
  $(step1__selects[0]).addClass("active");
})
$(".select-basic").click(function () {
  toggleModel();
  $(step1__selects[1]).addClass("active");
})
$(".select-collab").click(function () {
  toggleModel();
  $(step1__selects[2]).addClass("active");
})
$(".select-pro").click(function () {
  toggleModel();
  $(step1__selects[3]).addClass("active");
})
// model.removeClass("hide");
// $(step1__selects[0]).addClass("active");
// nextBtnFirst.click();
// $(step2__selects[0]).addClass("active");
// nextBtnSec.click();