const price_table_windows = $(".price-table-window");
const price_table_windows_pc = $(".price-table-window").not(".price-table-window_mobile");
const price_table_windows_mobile = $(".price-table-window.price-table-window_mobile");
const close_popup = $(".price-table-popup .close-popup");
const price_table_popup = $(".price-table-popup");

let windows_number = price_table_windows_pc.length;
let mobile_windows_number = 4;
let current_window = 0;
let current_mobile_window = 0;

const nav_next = $(".price-table-window-control__nav.price-table-window-control__nav_next");
const nav_prev = $(".price-table-window-control__nav.price-table-window-control__nav_prev");

//next popup
nav_next.click(() => {
  let activePopup = $(".price-table-window.price-table-window_mobile.active");
  if (activePopup.hasClass("price-table-window_mobile")) {
    let popups = $('[data-bz-popup-data="' + activePopup.data("bz-popup-data") + '"]');
    $(popups[current_mobile_window]).removeClass("active");

    current_mobile_window--;
    if (current_mobile_window < 0) {
      current_mobile_window = mobile_windows_number - 1;
    }

    $(popups[current_mobile_window]).addClass("active");
  }
  else {
    price_table_windows.each((index) => {
      $(price_table_windows[index]).removeClass("active");
    });

    current_window--;

    if (current_window < 0) {
      current_window = windows_number - 1;
    }

    $(price_table_windows[current_window]).addClass("active");
  }
})

//previous popup
nav_prev.click(() => {
  let activePopup = $(".price-table-window.price-table-window_mobile.active");
  if (activePopup.hasClass("price-table-window_mobile")) {
    let popups = $('[data-bz-popup-data="' + activePopup.data("bz-popup-data") + '"]');
    $(popups[current_mobile_window]).removeClass("active");

    current_mobile_window++;
    if (current_mobile_window > (mobile_windows_number - 1)) {
      current_mobile_window = 0;
    }

    $(popups[current_mobile_window]).addClass("active");
  }
  else {
    price_table_windows.each((index) => {
      $(price_table_windows[index]).removeClass("active");
    });

    current_window++;

    if (current_window > (windows_number - 1)) {
      current_window = 0;
    }

    $(price_table_windows[current_window]).addClass("active");
  }

})

//close popup
close_popup.click(() => {
  price_table_popup.addClass("hide");
  current_window = 0;
  current_mobile_window = 0;
})

// message click 
$(".price-table-supply__message").click((e) => {
  var width = $(window).width();
  price_table_popup.removeClass("hide");
  price_table_windows.each((index) => {
    $(price_table_windows[index]).removeClass("active");
  });
  if (width >= 992) {
    $('[data-bz-popup-data="' + $(e.currentTarget).data("popup-icon-data") + '"]').addClass("active");
  }
  else {
    switch ($(e.currentTarget).data("mobile-popup-category-data")) {
      case "FREE": $('[data-bz-popup-data="' + $(e.currentTarget).data("popup-icon-data") + '/PROJECT"]:first').addClass("active"); current_mobile_window = 0; break;
      case "BASIC": $('[data-bz-popup-data="' + $(e.currentTarget).data("popup-icon-data") + '/PROJECT"]').eq(1).addClass("active"); current_mobile_window = 1; break;
      case "STANDARD": $('[data-bz-popup-data="' + $(e.currentTarget).data("popup-icon-data") + '/PROJECT"]').eq(2).addClass("active"); current_mobile_window = 2; break;
      case "PROFESSIONAL": $('[data-bz-popup-data="' + $(e.currentTarget).data("popup-icon-data") + '/PROJECT"]:last').addClass("active"); current_mobile_window = 3; break;
      default: break;
    }
  }
})



// responsive window popup
$(window).resize(function () {
  var width = $(window).width();
  if (width >= 992) {
    let activePopup = $(".price-table-window.price-table-window_mobile.active");
    if (activePopup.length) {
      activePopup.removeClass("active");
      switch (activePopup.data("bz-popup-data")) {
        case "COLLABORATION/PROJECT":
          $('[data-bz-popup-data="COLLABORATION"]').addClass("active"); break;
        case "TASKS_AND_PROJECTS/PROJECT":
          $('[data-bz-popup-data="TASKS_AND_PROJECTS"]').addClass("active"); break;
        case "CRM/PROJECT":
          $('[data-bz-popup-data="CRM"]').addClass("active"); break;
        case "DRIVE/PROJECT":
          $('[data-bz-popup-data="DRIVE"]').addClass("active"); break;
        case "CONTACT_CENTER/PROJECT":
          $('[data-bz-popup-data="CONTACT_CENTER"]').addClass("active"); break;
        case "WEBSITE_BUILDER/PROJECT":
          $('[data-bz-popup-data="WEBSITE_BUILDER"]').addClass("active"); break;
        case "ONLINE_STORE/PROJECT":
          $('[data-bz-popup-data="ONLINE_STORE"]').addClass("active"); break;
        case "MARKETING/PROJECT":
          $('[data-bz-popup-data="MARKETING"]').addClass("active"); break;
        case "ONLINE_DOCUMENTS/PROJECT":
          $('[data-bz-popup-data="ONLINE_DOCUMENTS"]').addClass("active"); break;
        case "SALES_INTELLIGENCE/PROJECT":
          $('[data-bz-popup-data="SALES_INTELLIGENCE"]').addClass("active"); break;
        case "BUSINESS_PROCESS_AUTOMATION/PROJECT":
          $('[data-bz-popup-data="BUSINESS_PROCESS_AUTOMATION"]').addClass("active"); break;
        case "HR/PROJECT":
          $('[data-bz-popup-data="HR"]').addClass("active"); break;
        case "CUSTOMER_SUPPORT/PROJECT":
          $('[data-bz-popup-data="CUSTOMER_SUPPORT"]').addClass("active"); break;
        case "ADMINISTRATION/PROJECT":
          $('[data-bz-popup-data="ADMINISTRATION"]').addClass("active"); break;
        // ...
        default: break;
      }
      current_mobile_window = 0;
    }
  }
  else {
    let activePopup = $(".price-table-window.active");
    if (activePopup.length && !activePopup.hasClass("price-table-window_mobile")) {
      activePopup.removeClass("active");
      $('[data-bz-popup-data="' + activePopup.data("bz-popup-data") + '/PROJECT"]:first').addClass("active");
    }
  }
});

