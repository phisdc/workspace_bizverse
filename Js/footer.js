const footer_menus_expand = $(".footer-main-menu-section__icon_expand");
const footer_menus_list = $(".footer-main-menu-section__list");
const footer_menu_sections = $(".footer-main-menu__section_inner");
footer_menus_expand.each((index) => {
    $(footer_menus_expand[index]).click(() => {
        $(footer_menu_sections[index]).toggleClass("active");
    })
})