const header_menu = $(".header-menu-switch");
const menu_content = $(".header-content-slide__outer");
header_menu.click(() => {
    if (header_menu.hasClass("closed")) {
        header_menu.addClass("active");
        header_menu.removeClass("closed");
        menu_content.addClass("active");
        menu_content.css({"height" : "300px"});
    }
    else {
        header_menu.removeClass("active");
        header_menu.addClass("closed");
        menu_content.removeClass("active");
    }
})